

import numpy as np  # linear algebra
import pandas as pd  # data processing, CSV file I/O (e.g. pd.read_csv)
from sklearn.preprocessing import LabelEncoder
from lightgbm import LGBMRegressor
##Add decomposed components: PCA / ICA etc.
from sklearn.decomposition import PCA, FastICA
import lightgbm as lgb

from hyperopt import fmin, tpe, hp, STATUS_OK, Trials
from hyperopt.pyll.base import scope
from sklearn.svm import SVR
from sklearn.model_selection import cross_val_score
from sklearn.preprocessing import MinMaxScaler

# read datasets
PATH_TO_DATA = "/home/thusitha/work/bigdata/datasets/benz/"

N_PCA = 15
N_ICA = 15



def load_data():
    train = pd.read_csv(PATH_TO_DATA + 'train.csv')
    test = pd.read_csv(PATH_TO_DATA + 'test.csv')
    # process columns, apply LabelEncoder to categorical features
    columns_cat = []
    for c in train.columns:
        if train[c].dtype == 'object':
            columns_cat.append(c)
            lbl = LabelEncoder()
            lbl.fit(list(train[c].values) + list(test[c].values))
            train[c] = lbl.transform(list(train[c].values))
            test[c] = lbl.transform(list(test[c].values))
    # print shape
    print('Shape train: {}\nShape test: {}'.format(train.shape, test.shape))
    return train,test,columns_cat

def scale_data(train:pd.DataFrame, test:pd.DataFrame):
    clf = MinMaxScaler()
    data_train = train.drop(['y', 'ID'], axis=1)
    cols = data_train.columns
    clf.fit(data_train)
    train[cols] = clf.transform(data_train)
    test[cols] = clf.transform(test[cols])
    return train, test


def ica_cols(train:pd.DataFrame, test:pd.DataFrame):
    ica = FastICA(n_components=N_ICA, random_state=420)
    ica2_results_train = ica.fit_transform(train.drop(["y"], axis=1))
    ica2_results_test = ica.transform(test)
    return ica2_results_train, ica2_results_test


def pca_cols(train:pd.DataFrame, test:pd.DataFrame):
    # PCA
    pca = PCA(n_components=N_PCA, random_state=420)
    pca2_results_train = pca.fit_transform(train.drop(["y"], axis=1))
    pca2_results_test = pca.transform(test)
    return pca2_results_train, pca2_results_test


def score(params):
    print("Training with params : ")
    print(params)
    global min_err
    print("Train Data:" + str(X.shape))
    clf = SVR(kernel=params['kernel'], degree=params['degree'], C=params['C'], epsilon=params['epsilon'])
    scores = cross_val_score(clf, X, y, cv=5, n_jobs=-1)
    print(scores)
    mean_score = 1 - np.mean(scores) #SVR returns the R^2, [0,1] 1 is better. So minimize the 1- R^2, 0 is better.
    if (mean_score<min_err):
        min_err = mean_score
    print("\tScore {0}\n\n".format(mean_score))
    return {'loss': mean_score, 'status': STATUS_OK}


def optimize(trials):
    space = {
        'degree': scope.int(hp.quniform('degree', 1, 2, 1)),
        #'epsilon': hp.quniform('epsilon', 0.005, 0.02, 0.005), hp.loguniform
        'epsilon': hp.loguniform('epsilon', 1, 5),
        'C': hp.loguniform('C', 1, 10),
        'kernel': hp.choice('kernel',['rbf'])
    }

    best = fmin(score, space, algo=tpe.suggest, max_evals=500, trials=trials)
    print(best)
    return best

############################################################################

train,test,columns_cat = load_data()

pca2_results_train, pca2_results_test = pca_cols(train,test)
ica2_results_train, ica2_results_test = ica_cols(train,test)


# drop everything. We train a model only with PCA and ICA comumns
train = train[['ID', 'y']]
test = test[['ID']]

for i in range(1, N_PCA + 1):
    train['pca_' + str(i)] = pca2_results_train[:, i - 1]
    test['pca_' + str(i)] = pca2_results_test[:, i - 1]

for i in range(1, N_ICA + 1):
    train['ica_' + str(i)] = ica2_results_train[:, i - 1]
    test['ica_' + str(i)] = ica2_results_test[:, i - 1]

train, test = scale_data(train, test)

y = train['y']
X = train.drop(['y','ID'], axis=1, inplace=False)


min_err = 1000000
trials = Trials()
param = optimize(trials)

print("min " +str( min_err))
print("End of CV")
