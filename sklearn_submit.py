import numpy as np  # linear algebra
import pandas as pd  # data processing, CSV file I/O (e.g. pd.read_csv)
from sklearn.preprocessing import LabelEncoder
from sklearn.decomposition import PCA, FastICA
import lightgbm as lgb
from lightgbm import cv

# read datasets
PATH_TO_DATA = "/home/thusitha/work/bigdata/datasets/benz/"


N_PCA = 10
N_ICA = 10


params = {}
params['learning_rate'] = 0.13
params['boosting_type'] = 'dart'
params['drop_rate'] = 0.005
params['objective'] = 'regression'
params['metric'] = 'mse'
params['sub_feature'] = 0.6
params['num_leaves'] = 4
params['n_estimators'] = 500
params['lambda_l2'] = 0.05
params['max_depth'] = 60
params['min_data'] = 10
params['min_hessian'] = 0.6
params['num_threads'] = 8


def load_data():
    train = pd.read_csv(PATH_TO_DATA + 'train.csv')
    test = pd.read_csv(PATH_TO_DATA + 'test.csv')
    # process columns, apply LabelEncoder to categorical features
    columns_cat = []
    for c in train.columns:
        if train[c].dtype == 'object':
            columns_cat.append(c)
            lbl = LabelEncoder()
            lbl.fit(list(train[c].values) + list(test[c].values))
            train[c] = lbl.transform(list(train[c].values))
            test[c] = lbl.transform(list(test[c].values))
    # print shape
    print('Shape train: {}\nShape test: {}'.format(train.shape, test.shape))
    return train,test,columns_cat

def ica_cols(train:pd.DataFrame, test:pd.DataFrame):
    ica = FastICA(n_components=N_ICA, random_state=420)
    ica2_results_train = ica.fit_transform(train.drop(["y"], axis=1))
    ica2_results_test = ica.transform(test)
    return ica2_results_train, ica2_results_test


def pca_cols(train:pd.DataFrame, test:pd.DataFrame):
    # PCA
    pca = PCA(n_components=N_PCA, random_state=420)
    pca2_results_train = pca.fit_transform(train.drop(["y"], axis=1))
    pca2_results_test = pca.transform(test)
    return pca2_results_train, pca2_results_test

def train_model(d_train):
    print("Training with params : ")
    print(params)
    num_rounds = int(params['n_estimators'])
    del params['n_estimators']
    clf = lgb.train(params=params, train_set=d_train, num_boost_round=num_rounds, categorical_feature=columns_cat,
                 early_stopping_rounds=50)
    return clf


############################################################################

train,test,columns_cat = load_data()

pca2_results_train, pca2_results_test = pca_cols(train,test)
ica2_results_train, ica2_results_test = ica_cols(train,test)

# Append decomposition components to datasets
for i in range(1, N_PCA + 1):
    train['pca_' + str(i)] = pca2_results_train[:, i - 1]
    test['pca_' + str(i)] = pca2_results_test[:, i - 1]

for i in range(1, N_ICA + 1):
    train['ica_' + str(i)] = ica2_results_train[:, i - 1]
    test['ica_' + str(i)] = ica2_results_test[:, i - 1]

y = train['y']
X = train.drop(['y'], axis=1, inplace=False)
d_train = lgb.Dataset(X, label=y, categorical_feature=columns_cat)
clf = train_model(d_train)

# Generate Predictions
test['y'] = clf.predict(test)
output = test[['ID', 'y']]

# Save predictions to 'output.csv'
output.to_csv(PATH_TO_DATA + 'output_pca.csv', index=False)