import pandas as pd#
import lightgbm as lgb
import numpy as np

import mca

from hyperopt import fmin, tpe, hp, STATUS_OK, Trials
from hyperopt.pyll.base import scope


# do some feature engineering

PATH_TO_DATA = "/home/thusitha/work/bigdata/datasets/benz/"
MCA_VARIANCE = 0.95


def feature_enhance(df:pd.DataFrame):
    cols = [str(x) for x in df.columns]
    mca_df = mca.mca(df, cols)
    d = mca_df.fs_r(MCA_VARIANCE)
    n_cols_mca = d.shape[1]
    col_names_mca = [ (str(x)+"_mca") for x in range(1,n_cols_mca+1) ]
    df[col_names_mca] = pd.DataFrame(d, columns=col_names_mca)
    return (df, col_names_mca)


#everything is categorical. Convert letters to integers first.
#handle  everything about deat acleaning
def load_data(df:pd.DataFrame):
    #get the y col out
    y = df['y']
    df.drop(['y'], axis=1, inplace=True)
    cols = [str(x) for x in df.columns]
    df['dummy'] = 0.5
    for c in cols:
        df[c] = df[c].astype('category').cat.codes
    df, new_cols = feature_enhance(df)
    d_train = lgb.Dataset(df, label=y, categorical_feature=cols)
    return (d_train,cols,new_cols)

def score(params):
    print("Training with params : ")
    print(params)
    global min_err
    num_rounds = int(params['n_estimators'])
    del params['n_estimators']
    clf = lgb.cv(params=params, train_set=d_train, num_boost_round=num_rounds, categorical_feature=columns_cat,
                 early_stopping_rounds=20)
    mean_score = clf['l2-mean'][-1]
    if (mean_score<min_err):
        min_err = mean_score
    print("\tScore {0}\n\n".format(mean_score))
    return {'loss': mean_score, 'status': STATUS_OK}


def optimize():
    space = {
        'num_leaves': scope.int(hp.quniform('num_leaves', 2, 8, 2)),
        'learning_rate': hp.quniform('learning_rate', 0.10, 0.20, 0.01),
       # 'min_data': scope.int(hp.quniform('min_data', 10, 30, 10)),
        'min_data': 10,
        'max_depth': scope.int(hp.quniform('max_depth', 40, 100, 20)),
        'min_hessian': hp.quniform('min_hessian', 0.5, 1.0, 0.1),
        'sub_feature': hp.quniform('sub_feature', 0.2, 0.6, 0.2),
        'lambda_l2': hp.quniform('lambda_l2', 0.0, 0.2, 0.05),
        'boosting_type': 'dart',
	'drop_rate': hp.quniform('drop_rate', 0.000, 0.06, 0.002),
        'metric': 'mse',
        'objective': 'regression',
        'num_threads': 8,
        'n_estimators': scope.int(hp.quniform('n_estimators', 400, 800, 100))
    }

    best = fmin(score, space, algo=tpe.suggest, max_evals=500)
    print(best)
    return best


train = pd.read_csv(PATH_TO_DATA+'train.csv')
d_train,columns_cat, cols_new = load_data(train)
min_err = 1000000
param = optimize()

print("min " +str( min_err))
print("End of CV")


