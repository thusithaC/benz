
import pandas as pd#
import lightgbm as lgb
import numpy as np
import mca

PATH_TO_DATA = "/home/thusitha/work/bigdata/datasets/benz/"
MCA_VARIANCE = 0.95

params = {}
params['learning_rate'] = 0.13
params['boosting_type'] = 'dart'
params['drop_rate'] = 0.005
params['objective'] = 'regression'
params['metric'] = 'mse'
params['sub_feature'] = 0.6
params['num_leaves'] = 4
params['n_estimators'] = 500
params['lambda_l2'] = 0.05
params['max_depth'] = 60
params['min_data'] = 10
params['min_hessian'] = 0.6
params['num_threads'] = 8

def feature_enhance(df:pd.DataFrame):
    cols = [str(x) for x in df.columns]
    mca_df = mca.mca(df, cols)
    d = mca_df.fs_r(MCA_VARIANCE)
    n_cols_mca = d.shape[1]
    col_names_mca = [ (str(x)+"_mca") for x in range(1,n_cols_mca+1) ]
    df[col_names_mca] = pd.DataFrame(d, columns=col_names_mca)
    return (df, col_names_mca)

#everything is categorical. Convert letters to integers first.
#handle  everything about deat acleaning
def load_data(df_train:pd.DataFrame, df_test:pd.DataFrame, train_idx:list, val_idx:list):
    #get the y col out
    y_data = df_train['y']
    df_train.drop(['y'], axis=1, inplace=True)
    len_train = df_train.shape[0]
    cols = [str(x) for x in df_train.columns]
    df = pd.concat([df_train,df_test],axis=0) # create a combined df for data cleaning
    df['dummy'] = 0.5
    for c in cols:
        df[c] = df[c].astype('category').cat.codes
    new_cols = []
    df, new_cols = feature_enhance(df)
    df_train, df_test = df[:len_train], df[len_train:]
    df_val = df_train.loc[train_idx]
    d_train = lgb.Dataset(df_train.loc[train_idx,:], label=y_data.loc[train_idx], categorical_feature=cols)
    #d_test = lgb.Dataset(df_test, categorical_feature=cols)
    d_val = lgb.Dataset(df_train.loc[val_idx,:], label=y_data.loc[val_idx], categorical_feature=cols, reference=d_train)
    return (d_train, d_val, df_test, cols, new_cols)

def train(params, d_train:lgb.Dataset, d_val:lgb.Dataset):
    print("Training with params : ")
    print(params)
    num_rounds = int(params['n_estimators'])
    del params['n_estimators']
    clf = lgb.train(params=params, train_set=d_train, num_boost_round=num_rounds, categorical_feature=list(columns))
    return clf


def predict(clf:lgb.Booster, d_test:lgb.Dataset):
    print("Training with params : ")
    y_predict = clf.predict(d_test)
    return y_predict

df_train = pd.read_csv(PATH_TO_DATA+'train.csv')
df_test = pd.read_csv(PATH_TO_DATA+'test.csv')
data_length = df_train.shape[0]
val_split = data_length
train_idx = list(range(0,val_split))
val_idx = list(range(val_split,data_length))

d_train, d_val, d_test, columns, new_cols = load_data(df_train, df_test, train_idx, val_idx)
clf = train(params, d_train, d_val)

#Generate Predictions
df_test['y']=clf.predict(d_test)
output=df_test[['ID', 'y']]

#Save predictions to 'output.csv'
print("Saving results...")
output.to_csv(PATH_TO_DATA + 'output.csv', index=False)
