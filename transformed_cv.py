
import pandas as pd  # data processing, CSV file I/O (e.g. pd.read_csv)
from sklearn.preprocessing import LabelEncoder
import lightgbm as lgb
import numpy as np
from sklearn.metrics import r2_score
from hyperopt import fmin, tpe, hp, STATUS_OK, Trials
from hyperopt.pyll.base import scope


# read datasets
PATH_TO_DATA = "/home/thusitha/work/bigdata/datasets/benz/"


# self-defined eval metric
# f(preds: array, train_data: Dataset) -> name: string, value: array, is_higher_better: bool
def r2(preds, train_data):
    labels = train_data.get_label()
    return 'r2', r2_score(labels, preds), True


def load_data():
    train = pd.read_csv(PATH_TO_DATA + 'train.csv')
    test = pd.read_csv(PATH_TO_DATA + 'test.csv')
    # process columns, apply LabelEncoder to categorical features
    columns_cat = []
    for c in train.columns:
        if train[c].dtype == 'object':
            columns_cat.append(c)
            lbl = LabelEncoder()
            lbl.fit(list(train[c].values) + list(test[c].values))
            train[c] = lbl.transform(list(train[c].values))
            test[c] = lbl.transform(list(test[c].values))
    # print shape
    print('Shape train: {}\nShape test: {}'.format(train.shape, test.shape))
    return train,test,columns_cat


def score(params):
    print("Training with params : ")
    print(params)
    global min_err
    num_rounds = int(params['n_estimators'])
    del params['n_estimators']
    clf = lgb.cv(params=params, train_set=d_train, num_boost_round=num_rounds, categorical_feature=columns_cat,
                 early_stopping_rounds=50, feval=r2)
    mean_score = 1 - clf['r2-mean'][-1] #minimizing negative R2,
    if (mean_score<min_err):
        min_err = mean_score
    print("\t1-R2 Score {0}\n\n".format(mean_score))
    return {'loss': mean_score, 'status': STATUS_OK}


def optimize():
    space = {
        'num_leaves': scope.int(hp.quniform('num_leaves', 5, 20, 5)),
        'learning_rate': hp.quniform('learning_rate', 0.002, 0.008, 0.002),
        'min_data': scope.int(hp.quniform('min_data', 10, 30, 10)),
        #'min_data': 10,
        'max_depth': scope.int(hp.quniform('max_depth', 5, 50, 5)),
        'min_data_in_leaf': scope.int(hp.quniform('min_data_in_leaf', 4, 20, 4)),
        ' max_bin': scope.int(hp.quniform('max_bin', 10, 100, 10)),
        'min_hessian': hp.quniform('min_hessian', 0.5, 1.0, 0.1),
        'min_sum_hessian_in_leaf': hp.quniform('min_sum_hessian_in_leaf', 0.0005, 0.0015, 0.0005),
        'sub_feature': hp.quniform('sub_feature', 0.2, 1.0, 0.2),
        'lambda_l2': hp.quniform('lambda_l2', 0.0, 0.2, 0.05),
        'boosting_type': 'gbdt',
	    #'drop_rate': hp.quniform('drop_rate', 0.000, 0.06, 0.002),
        #'metric': 'mse',
        'objective': 'regression',
        'num_threads': 8,
        'n_estimators': scope.int(hp.quniform('n_estimators', 400, 800, 100))
    }
    best = fmin(score, space, algo=tpe.suggest, max_evals=1000)
    print(best)
    return best



############################################################################

train,test,columns_cat = load_data()

y = train['y']
X = train.drop(['y'], axis=1, inplace=False)
d_train = lgb.Dataset(X, label=y, categorical_feature=columns_cat)


min_err = 1000000
param = optimize()

print("min " +str( min_err))
print("End of CV")
