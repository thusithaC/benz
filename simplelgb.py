import pandas as pd#
import lightgbm as lgb
import numpy as np

from hyperopt import fmin, tpe, hp, STATUS_OK, Trials
from hyperopt.pyll.base import scope


PATH_TO_DATA = "/home/thusitha/work/bigdata/datasets/benz/"


import matplotlib.pyplot as plt

#everything is categorical. Convert letters to integers first.
#handle  everything about deat acleaning
def load_data(df):
    #get the y col out
    y = df['y']
    df.drop(['y'], axis=1, inplace=True)
    cols = df.columns
    df['dummy'] = 0.5
    for c in cols:
        df[c] = df[c].astype('category').cat.codes
    d_train = lgb.Dataset(df, label=y, categorical_feature=list(cols))
    return (d_train,cols)

def score(params):
    print("Training with params : ")
    print(params)
    global min_err
    num_rounds = int(params['n_estimators'])
    del params['n_estimators']
    clf = lgb.cv(params=params, train_set=d_train, num_boost_round=num_rounds, categorical_feature=list(columns),
                 early_stopping_rounds=40)
    mean_score = clf['l2-mean'][-1]
    if (mean_score<min_err):
        min_err = mean_score
    print("\tScore {0}\n\n".format(mean_score))
    return {'loss': mean_score, 'status': STATUS_OK}


def optimize():
    space = {
        'num_leaves': scope.int(hp.quniform('num_leaves', 5, 10, 5)),
        'learning_rate': hp.quniform('learning_rate', 0.2, 0.40, 0.05),
       # 'min_data': scope.int(hp.quniform('min_data', 10, 30, 10)),
        'min_data': 10,
        'max_depth': scope.int(hp.quniform('max_depth', 100, 200, 50)),
        'min_hessian': hp.quniform('min_hessian', 0.5, 0.7, 0.1),
       # 'sub_feature': hp.quniform('sub_feature', 0.8, 1, 0.2),
        'lambda_l2': hp.quniform('lambda_l2', 0.1, 0.4, 0.1),
        'boosting_type': 'dart',
	'drop_rate': hp.quniform('drop_rate', 0.000, 0.02, 0.005),
        'metric': 'mse',
        'objective': 'regression',
        'num_threads': 8,
        'n_estimators': 1000
    }

    best = fmin(score, space, algo=tpe.suggest, max_evals=500)
    print(best)
    return best


train = pd.read_csv(PATH_TO_DATA+'train.csv')
d_train,columns = load_data(train)
min_err = 1000000
param = optimize()

print("min " +str( min_err))
print("End of CV")


