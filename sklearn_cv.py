import numpy as np  # linear algebra
import pandas as pd  # data processing, CSV file I/O (e.g. pd.read_csv)
from sklearn.preprocessing import LabelEncoder
from lightgbm import LGBMRegressor
##Add decomposed components: PCA / ICA etc.
from sklearn.decomposition import PCA, FastICA
import lightgbm as lgb

from hyperopt import fmin, tpe, hp, STATUS_OK, Trials
from hyperopt.pyll.base import scope


# read datasets
PATH_TO_DATA = "/home/thusitha/work/bigdata/datasets/benz/"

N_PCA = 10
N_ICA = 10



def load_data():
    train = pd.read_csv(PATH_TO_DATA + 'train.csv')
    test = pd.read_csv(PATH_TO_DATA + 'test.csv')
    # process columns, apply LabelEncoder to categorical features
    columns_cat = []
    for c in train.columns:
        if train[c].dtype == 'object':
            columns_cat.append(c)
            lbl = LabelEncoder()
            lbl.fit(list(train[c].values) + list(test[c].values))
            train[c] = lbl.transform(list(train[c].values))
            test[c] = lbl.transform(list(test[c].values))
    # print shape
    print('Shape train: {}\nShape test: {}'.format(train.shape, test.shape))
    return train,test,columns_cat

def ica_cols(train:pd.DataFrame, test:pd.DataFrame):
    ica = FastICA(n_components=N_ICA, random_state=420)
    ica2_results_train = ica.fit_transform(train.drop(["y"], axis=1))
    ica2_results_test = ica.transform(test)
    return ica2_results_train, ica2_results_test


def pca_cols(train:pd.DataFrame, test:pd.DataFrame):
    # PCA
    pca = PCA(n_components=N_PCA, random_state=420)
    pca2_results_train = pca.fit_transform(train.drop(["y"], axis=1))
    pca2_results_test = pca.transform(test)
    return pca2_results_train, pca2_results_test


def score(params):
    print("Training with params : ")
    print(params)
    global min_err
    num_rounds = int(params['n_estimators'])
    del params['n_estimators']
    clf = lgb.cv(params=params, train_set=d_train, num_boost_round=num_rounds, categorical_feature=columns_cat,
                 early_stopping_rounds=50)
    mean_score = clf['l2-mean'][-1]
    if (mean_score<min_err):
        min_err = mean_score
    print("\tScore {0}\n\n".format(mean_score))
    return {'loss': mean_score, 'status': STATUS_OK}


def optimize():
    space = {
        'num_leaves': scope.int(hp.quniform('num_leaves', 2, 14, 2)),
        'learning_rate': hp.quniform('learning_rate', 0.016, 0.026, 0.002),
        'min_data': scope.int(hp.quniform('min_data', 10, 25, 5)),
        #'min_data': 10,
        'max_depth': scope.int(hp.quniform('max_depth', 20, 60, 10)),
        'min_data_in_leaf': scope.int(hp.quniform('min_data_in_leaf', 22, 28, 2)),
        ' max_bin': scope.int(hp.quniform('max_bin', 20, 50, 10)),
        'min_hessian': hp.quniform('min_hessian', 0.7, 1.0, 0.1),
        'min_sum_hessian_in_leaf': hp.quniform('min_sum_hessian_in_leaf', 0.0005, 0.0015, 0.0005),
        'sub_feature': hp.quniform('sub_feature', 0.2, 0.8, 0.2),
        'lambda_l2': hp.quniform('lambda_l2', 0.005, 0.03, 0.02),
        'boosting_type': 'gbdt',
	    #'drop_rate': hp.quniform('drop_rate', 0.000, 0.06, 0.002),
        'metric': 'mse',
        'objective': 'regression',
        'num_threads': 8,
        'n_estimators': scope.int(hp.quniform('n_estimators', 800, 1200, 100))
    }

    best = fmin(score, space, algo=tpe.suggest, max_evals=1000)
    print(best)
    return best

############################################################################

train,test,columns_cat = load_data()

pca2_results_train, pca2_results_test = pca_cols(train,test)
ica2_results_train, ica2_results_test = ica_cols(train,test)


# Append decomposition components to datasets
for i in range(1, N_PCA + 1):
    train['pca_' + str(i)] = pca2_results_train[:, i - 1]
    test['pca_' + str(i)] = pca2_results_test[:, i - 1]

for i in range(1, N_ICA + 1):
    train['ica_' + str(i)] = ica2_results_train[:, i - 1]
    test['ica_' + str(i)] = ica2_results_test[:, i - 1]

y = train['y']
X = train.drop(['y'], axis=1, inplace=False)
d_train = lgb.Dataset(X, label=y, categorical_feature=columns_cat)


min_err = 1000000
param = optimize()

print("min " +str( min_err))
print("End of CV")
